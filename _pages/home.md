---
layout: splash
permalink: /
header:
  overlay_color: "#5e616c"
excerpt: >
  A flexible two-column Jekyll theme. Perfect for building personal sites, blogs, and portfolios.<br />
  <small><a href="https://github.com/mmistakes/minimal-mistakes/releases/tag/4.14.1">Latest release v4.14.1</a></small>
feature_row:
  - alt: "customizable"
    title: "Super customizable"
    excerpt: "Everything from the menus, sidebars, comments, and more can be configured or set with YAML Front Matter."
    url: "/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - alt: "fully responsive"
    title: "Responsive layouts"
    excerpt: "Built with HTML5 + CSS3. All layouts are fully responsive with helpers to augment your content."
    url: "/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
  - alt: "100% free"
    title: "100% free"
    excerpt: "Free to use however you want under the MIT License. Clone it, fork it, customize it... whatever!"
    url: "/"
    btn_class: "btn--primary"
    btn_label: "Learn more"
---

{% include feature_row %}
